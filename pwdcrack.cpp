#include "crypto.hpp"
#include <iostream>
#include <sstream>
#include <string>

using namespace std;



int main() {
  stringstream ss;
  string hash = "ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6";
  string salt = "Saltet til Ola";
  char first = 'A';
  while (first <= 'z') {
    string combination = "";
    combination += first;
    cout << combination << endl;
    if (!Crypto::hex(Crypto::pbkdf2(combination, salt)).compare(hash)) {
      cout << "Password found:" << endl;
      cout << combination << endl;
      return 0;
    }
    char second = 'A';
    while (second <= 'z') {
      combination = "";
      combination += first;
      combination += second;
      cout << combination << endl;
      if (!Crypto::hex(Crypto::pbkdf2(combination, salt)).compare(hash)) {
        cout << "Password found:" << endl;
        cout << combination << endl;
        return 0;
      }
      char third = 'A';
      while (third <= 'z') {
        combination = "";
        combination += first;
        combination += second;
        combination += third;
        cout << combination << endl;
        if (!Crypto::hex(Crypto::pbkdf2(combination, salt)).compare(hash)) {
          cout << "Password found:" << endl;
          cout << combination << endl;
          return 0;
        }
        if (third == 'Z') {
          third = 'a';
          continue;
        }
        third++;
      }
      if (second == 'Z') {
        second = 'a';
        continue;
      }
      second++;
    }
    if (first == 'Z') {
      first = 'a';
      continue;
    }
    first++;
  }
  return 0;
}
